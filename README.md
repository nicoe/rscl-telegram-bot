
A Telegram bot about Standard de Liège
======================================

# Commands

* `/subscribe`: Subscribe to scores change messages
* `/unsubscribe`: Unsubscribe from score change messages

# Messages

* `score`: Send the last score
* `standing`: Send the current standing
