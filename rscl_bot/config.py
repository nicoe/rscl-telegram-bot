import json
import os.path


CONFIG_FILE = '~/.var/standard-bot'
CONFIG = {}


class Encoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, set):
            return {
                '__set__': True,
                'value': list(o),
                }
        return super().default(o)


def object_hook(dct):
    if dct.get('__set__'):
        return set(dct['value'])
    return dct


def save():
    with open(os.path.expanduser(CONFIG_FILE), 'w') as fd:
        fd.write(json.dumps(CONFIG, cls=Encoder))


def load():
    CONFIG.update({
            'subscribers': set(),
            })

    config_path = os.path.expanduser(CONFIG_FILE)
    if os.path.exists(config_path):
        with open(config_path) as fd:
            CONFIG.update(json.loads(fd.read(), object_hook=object_hook))
