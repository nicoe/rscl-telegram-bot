import io
import itertools
import json
import re
from dataclasses import dataclass

import requests
import lxml.html

from .config import CONFIG

MAX_CALLS = 5
RSCL_HANDLE = 'Standard_RSCL'
INIT_USER_URL = 'http://twitter.com/{user}'
POSITION_USER_URL = ('http://twitter.com/i/profiles/show/{user}/timeline/'
    'tweets?include_available_features=1&include_entities=1'
    '&max_position={position}&reset_error_state=false')

AUTHOR_XPATH = (
    ".//div[@class='content']/div[@class='stream-item-header']"
    "/a/span[contains(@class, 'username')]")
TEXT_XPATH = ".//div[@class='content']/div[@class='js-tweet-text-container']"

MATCH_RE = re.compile(r'^([0-9]*) *- *([0-9]*).* (#[A-Z]{6})?')
END_OF_MATCH_RE = re.compile(r'^[Ff]ull [Tt]ime')
END_OF_MATCH_GOAL_LINE_RE = re.compile(
    r'.*[0-9]*’.*: *([0-9]*) *- *([0-9]*)')


def get_tweets(user, position=None):
    if position is None:
        url = INIT_USER_URL.format(user=user)
    else:
        url = POSITION_USER_URL.format(user=user, position=position)
    response = requests.get(url)
    if position is None:
        tweets = from_html(response.text)
    else:
        tweets = from_html(response.json()['items_html'])

    return tweets


def from_html(html):
    tree = lxml.html.parse(io.StringIO(html))
    tweets = []
    for li in tree.xpath("//li[@data-item-type='tweet']"):
        first_div = next(li.iterchildren())
        # Skip user-pinned tweets
        if 'user-pinned' in first_div.attrib.get('class', ''):
            continue
        metadata = json.loads(li.attrib['data-suggestion-json'])
        authors = {e.text_content() for e in li.xpath(AUTHOR_XPATH)}
        text = li.xpath(TEXT_XPATH)
        if text:
            tweets.append(Tweet(
                    id=metadata['tweet_ids'], authors=authors,
                    text=text[0].text_content().strip()))

    return tweets


@dataclass
class Tweet:
    id: str
    text: str
    authors: set


class TwitterChecker:

    def __init__(self):
        self.latest_tweet = None
        self.latest_score = None

    def _send_message(self, bot, message):
        for subscriber in CONFIG['subscribers']:
            bot.send_message(subscriber, message)

    def _get_new_tweets(self):
        calls_count = 0
        new_tweets = []
        new_tweets_ids = set()
        position = None
        while (self.latest_tweet not in new_tweets_ids
                and calls_count < MAX_CALLS):
            tweets = get_tweets(RSCL_HANDLE, position)
            new_tweets.extend(itertools.takewhile(
                    lambda t: t.id != self.latest_tweet,
                    tweets))
            new_tweets_ids = {t.id for t in tweets}
            position = tweets[0].id
            calls_count += 1
            if self.latest_tweet is None:
                break
        if new_tweets:
            self.latest_tweet = new_tweets[0].id
        return new_tweets

    def send_last_score(self, bot, update):
        if self.latest_score:
            message = '{} - {} {}'.format(*self.latest_score)
        else:
            message = 'No score yet'
        bot.send_message(chat_id=update.message.chat_id, text=message)

    def check(self, bot, job):
        new_tweets = self._get_new_tweets()

        skip_goal = False
        for tweet in new_tweets:
            message = self.check_goal(tweet)
            if message and not skip_goal:
                self._send_message(bot, message)
                skip_goal = True
            message = self.check_eom(tweet)
            if message:
                self._send_message(bot, message)

    def check_goal(self, tweet):
        match = MATCH_RE.search(tweet.text)
        if match:
            score = match.groups()
            if (self.latest_score is None
                    or score[:2] != self.latest_score[:2]):
                self.latest_score = score
                return ' ⚽ {} - {} {}'.format(*score)
        return None

    def check_eom(self, tweet):
        match = END_OF_MATCH_RE.search(tweet.text)
        if match:
            game_hashtag = re.search(
                r'(#(STA[A-Z]{3})|([A-Z]{3}STA))', tweet.text)
            game_hashtag = '' if not game_hashtag else game_hashtag.group(1)
            for line in tweet.text.splitlines():
                match_line = END_OF_MATCH_GOAL_LINE_RE.search(line)
                if match_line:
                    self.latest_score = (
                        match_line.group(1), match_line.group(2), game_hashtag)
            return tweet.text
