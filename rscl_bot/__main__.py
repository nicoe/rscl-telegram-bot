import os
import sys

from .bot import setup, run


def main():
    if not os.environ.get('TELEGRAM_BOT_TOKEN'):
        print('TELEGRAM_BOT_TOKEN environment variable missing!')
        sys.exit(1)

    sys.exit(run(setup()))


if __name__ == '__main__':
    main()
