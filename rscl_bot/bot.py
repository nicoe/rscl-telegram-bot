import os
import logging

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters

import rscl_bot.config
from .config import CONFIG
from .twitter import TwitterChecker


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG)
TOKEN = os.environ.get('TELEGRAM_BOT_TOKEN')


def subscribe(bot, update):
    CONFIG['subscribers'].add(update.message.from_user.id)
    bot.send_message(
        chat_id=update.message.chat_id,
        text="""
        Thank you for subscribing to RSCL Facts!

        Type /unsubscribe to cancel your subscription.
        """)


def unsubscribe(bot, update):
    CONFIG['subscribers'].discard(update.message.from_user.id)
    bot.send_message(
        chat_id=update.message.chat_id,
        text="""
        Command not recognized. You will receive hourly updates!

        https://www.reddit.com/r/funny/comments/owx3v/so_my_little_cousin_posted_on_fb_that_he_was/
        """)


class MessageDispatcher(dict):

    def __call__(self, bot, update):
        text = update.message.text.lower()
        if text in self:
            self[text](bot, update)
        else:
            bot.send_message(chat_id=update.message.chat_id,
                text="Unrecognized message")


def setup():
    rscl_bot.config.load()
    updater = Updater(token=TOKEN)
    message_dispatcher = MessageDispatcher()

    subscribe_handler = CommandHandler('subscribe', subscribe)
    updater.dispatcher.add_handler(subscribe_handler)

    unsubscribe_handler = CommandHandler('unsubscribe', unsubscribe)
    updater.dispatcher.add_handler(unsubscribe_handler)

    twitter_events = TwitterChecker()
    message_dispatcher['score'] = twitter_events.send_last_score
    updater.job_queue.run_repeating(twitter_events.check, 60)
    updater.dispatcher.add_handler(MessageHandler(
            Filters.text, message_dispatcher))

    return updater


def run(updater):
    updater.start_polling()
    updater.idle()
    rscl_bot.config.save()
    return 0
