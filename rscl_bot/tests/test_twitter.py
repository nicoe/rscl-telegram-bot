import unittest
from unittest.mock import patch

from ..twitter import TwitterChecker, Tweet


class TwitterTest(unittest.TestCase):

    def setUp(self):
        self.twitter_checker = TwitterChecker()

    def test_get_new_tweets(self):
        with patch('rscl_bot.twitter.get_tweets') as getter:
            getter.side_effect = [
                # First call we receive 2 tweets
                [Tweet(id='1', text='1', authors={'a'}),
                    Tweet(id='0', text='0', authors={'a'}),
                    ],
                # There has been two updates after the first call
                [Tweet(id='3', text='3', authors={'a'}),
                    Tweet(id='2', text='2', authors={'a'}),
                    ],
                [Tweet(id='1', text='1', authors={'a'}),
                    Tweet(id='0', text='0', authors={'a'}),
                    ],
                # There has been two updates after the first call
                [Tweet(id='3', text='3', authors={'a'}),
                    Tweet(id='2', text='2', authors={'a'}),
                    ],
                ]

            self.twitter_checker._get_new_tweets()
            self.assertEqual(self.twitter_checker.latest_tweet, '1')
            self.assertEqual(getter.call_count, 1)

            self.twitter_checker._get_new_tweets()
            self.assertEqual(self.twitter_checker.latest_tweet, '3')
            self.assertEqual(getter.call_count, 3)

            self.twitter_checker._get_new_tweets()
            self.assertEqual(self.twitter_checker.latest_tweet, '3')
            self.assertEqual(getter.call_count, 4)

    def test_check_goal(self):
        tweet = Tweet(id='0', authors={'Standard_RSCL'},
            text='MATCHDAY #STAAND')
        self.assertIsNone(self.twitter_checker.check_goal(tweet))

        tweet = Tweet(id='1', authors={'Standard_RSCL'},
            text='1 - 0 Ciman croise sa frappe #STAAND')
        self.assertEqual(
            self.twitter_checker.check_goal(tweet),
            ' ⚽ 1 - 0 #STAAND')

        tweet = Tweet(id='2', authors={'Standard_RSCL'},
            text='1 - 0 Carte rouge pour S. Defour #STAAND')
        self.assertIsNone(self.twitter_checker.check_goal(tweet))

        tweet = Tweet(id='3', authors={'Standard_RSCL'},
            text='2 - 0 Igor De Camargo dévie #STAAND')
        self.assertEqual(
            self.twitter_checker.check_goal(tweet),
            ' ⚽ 2 - 0 #STAAND')

        tweet = Tweet(id='4', authors={'Standard_RSCL'},
            text="2 - 0 Toujours")
        self.assertIsNone(self.twitter_checker.check_goal(tweet))

    def test_check_eom(self):
        tweet = Tweet(id='0', authors={'Standard_RSCL'},
            text=(
                "Full Time | Standard de Liège – KV Kortrijk: 2-1 #STAKVK #JPL"
                "\n35’ : 0-1 Chevalier "
                "\n66’ : 1-1 @EmondRenaud (pen.) "
                "\n90’ + 2’ : 2-1 Luyindama"))
        self.assertIsNotNone(self.twitter_checker.check_eom(tweet))
        self.assertEqual(
            self.twitter_checker.latest_score, ('2', '1', '#STAKVK'))

        tweet = Tweet(id='1', authors={'Standard_RSCL'},
            text='1 - 0 Ciman croise sa frappe #STAAND')
        self.assertIsNone(self.twitter_checker.check_eom(tweet))
